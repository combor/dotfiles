sudo chown -R gitpod:gitpod ~/.aws
export VISUAL=vim
export EDITOR=vim
export GIT_EDITOR=vim
GIT_PS1_SHOWDIRTYSTATE=true
GIT_PS1_SHOWSTASHSTATE=true
GIT_PS1_SHOWUNTRACKEDFILES=true
export AWS_DEFAULT_REGION="eu-west-2"
export CHECKPOINT_DISABLE="true"
export PYTEST_ADDOPTS="--log-cli-level=INFO -s"
git config --global rebase.autosquash true
eval "$(thefuck --alias)"
